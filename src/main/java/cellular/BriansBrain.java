package cellular;

import java.util.Random;

import javax.tools.DocumentationTool.Location;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton{

    IGrid currentGeneration;
    
    public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

    @Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

    @Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

    @Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();

		for (int r = 0; r < numberOfRows(); r++){
			for (int c = 0; c < numberOfColumns(); c++){
				nextGeneration.set(r, c, getNextCell(r, c));
			}
		}

		currentGeneration = nextGeneration;
		
	}

    public CellState getNextCell(int row, int col){
        int numberAlive = countNeighbors(row, col, CellState.ALIVE);
        CellState deadOrAlive = currentGeneration.get(row, col);
        

        //If the cell is alive it will become dying and if cell is dying it will die
        if (deadOrAlive == CellState.ALIVE){
            return CellState.DYING;
        }else if (deadOrAlive == CellState.DYING){
            return CellState.DEAD;
        }
        //If the cell is already dead then check if it will ressurect
        else{
            if (numberAlive == 2){
                return CellState.ALIVE;
            }else{
                return CellState.DEAD;
            }
        }




		

        
    }

    private int countNeighbors(int row, int col, CellState state) {
		int neighbors = 0;

		for (int r = row-1; r < row + 2; r++){
			for (int c = col-1; c < col + 2; c ++){

				if (r < 0 || r >= currentGeneration.numRows() || c < 0 || c >= currentGeneration.numColumns()){
					neighbors += 0;
				} else if (r == row && c == col){
					neighbors += 0;
				} else {
					if (state == CellState.DEAD){
						if (currentGeneration.get(r, c) != state){
							neighbors += 1;
						}
					}else if (state == CellState.ALIVE){
						if (currentGeneration.get(r, c) == state){
							neighbors += 1;
						}
					}
				}

			}
		}
		
		return neighbors;
	}

    @Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
