package datastructure;

import cellular.CellState;
import java.util.ArrayList;

public class CellGrid implements IGrid {
    private ArrayList<CellState> grid;
    private int rows;
    private int columns;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.columns = columns;
        this.rows = rows;
        grid = new ArrayList<CellState>();

        for (int row = 0; row < rows; row++){
            for (int col = 0; col < columns; col ++){
                grid.add(initialState);
            }
        }

    }


    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row < 0 || row >= rows){
            throw new IndexOutOfBoundsException();
        }
        if (column < 0 || column >= columns){
            throw new IndexOutOfBoundsException();
        }

        grid.set(coordinateToIndex(row, column), element);
    }

    //function to get a location in the grid
    private int coordinateToIndex(int x, int y){
        return x + y * rows;
    }

    @Override
    public CellState get(int row, int column) {
        if (row < 0 || row >= rows){
            throw new IndexOutOfBoundsException();
        }
        if (column < 0 || column >= columns){
            throw new IndexOutOfBoundsException();
        }

        return grid.get(coordinateToIndex(row, column));
    }

    @Override
    public IGrid copy() {
        CellGrid newGrid = new CellGrid(rows, columns, null);
        
        for (int r = 0; r < rows; r++){
            for (int c = 0; c < columns; c++){
                newGrid.set(r, c, get(r, c));
            }
        }

        return newGrid;
    }
    
}
